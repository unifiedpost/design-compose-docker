# Using Proxy Server #

For docker proxy settings you can read https://docs.docker.com/network/proxy/

This guide will show how to make Design & Compose server work with a HTTP proxy server. Use this if your servers don't have access to the internet directly.
It uses the same technique as the configuration sample to build a scriptura.xml file.

To test if this image work a docker proxy server can be started.

## Building and running the server ##

```
docker build -t design-compose-proxy .
```

If you don't have a proxy server you can use this command to start one. It will start a squid proxy server on port 3128.
```
docker run -d --name squid-container -e TZ=UTC -p 3128:3128 ubuntu/squid:4.13-21.10_edge
```

You must specify your proxy settings using environment variables.
- ENV PROXY_HOST
- ENV PROXY_PORT

Replace 192.168.1.26 and 3128 with the actual values in the command bellow.
```
docker run -ePROXY_HOST=192.168.1.26 -ePROXY_PORT=3128 --name design-compose-server-proxy -p13542:13542 design-compose-proxy
```
