# Base image #

This is a docker image for the stand alone version of the one way sync.
It can sync a given remote idr path. It can be modified to sync more.

# Requirements #

This guide assumes basic knowledge of linux.

You will need to following:

1. **Docker** installation
2. A base **image with Java 11** (preferably AWS Corretto 11 JDK). The *java* command should be available on the path. If you do not have this base container or you are just experimenting you can use *amazoncorretto:11*.
3. **one-way-sync-standalone-integration.jar**, the one way sync stand alone jar file.
5. A text editor, preferably with docker syntax support

You will also need the following information for running the image
1. The remote IDR location to sync
2. The path on the docker host where the sync files must be store
3. A client id/secret pair of a user that has access to the files on the remote resource server

# Dockerfile #

This section describes the various parts of the Dockerfile.
Copy the **one-way-sync-standalone-integration.jar** into the one-way-sync folder next to the dockerfile.

You can build the image using docker build.
```
cd one-way-sync
docker build -t one-way-sync .
```

# Note on users #

We create a user with uuid 1000 and gid 1000. It is important to change this to a user on your docker host that should have control of the synced files.

## Building and running the one way sync ##

```
docker build -t one-way-sync .
```

In order to run the image you must replace some parts in the following command:

```
docker run -it -v /folder/on/dockerhost:/var/sync sync -c XXXXXXXXXX -s XYXYXYXYXYXYXYXYXYXYX -p idr://remote/idr/path
```

### -v /folder/on/dockerhost:/var/sync sync ###

This is a docker bind mount. It will make sure that the folder `/folder/on/dockerhost` is mounted under `/var/sync` in the docker container.
This folder will thus contain the synced resources. It should be writeable by the user with the uuid and gid choses in the docker image. Without changes this is 1000:1000.

### -c XXXXXXXXXX ###

This is an argument passed to the application. This is the client id.

### -s XYXYXYXYXYXYXYXYXYXYX ###

This is an argument passed to the application. This is the client secret.

### -p idr://remote/idr/path ###

This is an argument passed to the application. This is the remote idr path to sync.
