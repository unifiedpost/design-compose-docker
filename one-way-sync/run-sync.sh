#!/bin/bash

ORIGINGAL_ARGS="$@"

usage () {
    echo "Usage run-sync.sh -p idr-path -c client-id -s client-secret [-r remote server][-l local sync folder]"
    echo "Your container args are: $ORIGINGAL_ARGS"
    echo "Please check them"
    exit -1
}

CLIENTID=""
CLIENTSECRET=""
REMOTE="https://resource-app-eu1.scripturaengage.com"
IDRPATH=""
SYNCFOLDER="/var/sync"

# find config file, we merge it with our defaults
while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--idr-path)
            IDRPATH="$2"
            shift
            shift
            ;;
        -l|--path)
            SYNCFOLDER="$2"
            shift
            shift
            ;;
        -c|--client-id)
            CLIENTID="$2"
            shift
            shift
            ;;
        -s|--client-secret)
            CLIENTSECRET="$2"
            shift
            shift
            ;;
        -r|--remote)
            REMOTE="$2"
            shift
            shift
            ;;
        *)
            usage
            ;;
    esac
done

if [[ "$SYNCFOLDER" == "" ]]; then
   usage
fi

if [[ "$REMOTE" == "" ]]; then
   usage
fi

if [[ "$IDRPATH" == "" ]]; then
    usage
fi

if [[ "$CLIENTID" == "" ]]; then
    usage
fi

if [[ "$CLIENTSECRET" == "" ]]; then
    usage
fi

cp sync-properties-template.xml sync-properties.xml
sed -i "s|#REMOTE#|$REMOTE|g" sync-properties.xml
sed -i "s|#IDRPATH#|$IDRPATH|g" sync-properties.xml
sed -i "s|#CLIENTID#|$CLIENTID|g" sync-properties.xml
sed -i "s|#CLIENTSECRET#|$CLIENTSECRET|g" sync-properties.xml
sed -i "s|#SYNCFOLDER#|$SYNCFOLDER|g" sync-properties.xml

#cat sync-properties.xml
java -jar one-way-sync-standalone-integration.jar