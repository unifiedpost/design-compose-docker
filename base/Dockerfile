FROM amazonlinux:2 as builder
WORKDIR /tmp
RUN yum install -y unzip
COPY design-compose-21.04.zip design-compose-21.04.zip
RUN unzip design-compose-21.04.zip && \
    rm design-compose-21.04.zip

FROM amazoncorretto:11
WORKDIR /opt/design-compose
COPY --from=builder /tmp .
RUN yum install -y shadow-utils  && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    mkdir -p /var/lib/app && \
    groupadd --gid 1000 dcshe && \
    useradd --uid 1000 --gid dcshe --shell /bin/bash --base-dir /var/lib/app --create-home dcshe && \
    mkdir -p /var/lib/app/dcshe/design-compose && \
    chown -R 1000:1000 /var/lib/app/dcshe/design-compose
COPY --chown=1000:1000 license.lic /var/lib/app/dcshe/design-compose

USER 1000:1000
VOLUME /var/lib/app/dcshe/design-compose
VOLUME /tmp
ENV JVM_XMS="1024m"
ENV JVM_XMX="1024m"
ENV JAVA_OPTS=""
EXPOSE 13542
ENTRYPOINT ./StartServer --launcher.appendVmargs -vmargs \
    -Xms$JVM_XMS \
    -Xmx$JVM_XMX \
    $JAVA_OPTS